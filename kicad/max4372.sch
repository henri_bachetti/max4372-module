EESchema Schematic File Version 2
LIBS:max4372-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-ICs
LIBS:my-power-monitors
LIBS:max4372-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR01
U 1 1 593B1C34
P 4000 4100
F 0 "#PWR01" H 4000 3850 50  0001 C CNN
F 1 "GND" H 4000 3950 50  0000 C CNN
F 2 "" H 4000 4100 50  0000 C CNN
F 3 "" H 4000 4100 50  0000 C CNN
	1    4000 4100
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 593B1D63
P 4950 4000
F 0 "R1" V 5030 4000 50  0000 C CNN
F 1 "0.1" V 4950 4000 50  0000 C CNN
F 2 "Resistors_SMD:R_2010_HandSoldering" V 4880 4000 50  0001 C CNN
F 3 "" H 4950 4000 50  0000 C CNN
	1    4950 4000
	0    1    1    0   
$EndComp
$Comp
L CONN_01X07 P1
U 1 1 593B3352
P 3500 3700
F 0 "P1" H 3500 4100 50  0000 C CNN
F 1 "IN-OUT" V 3600 3700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x07" H 3500 3700 50  0001 C CNN
F 3 "" H 3500 3700 50  0000 C CNN
	1    3500 3700
	-1   0    0    -1  
$EndComp
Text Notes 3550 5100 0    60   ~ 0
VCC = 5V, Gain = 50, ADC = 10bits\nVout max = 5V\nR1 = 100mΩ : Imax = 1A, resolution = 4.88mA\nR1 = 1Ω : Imax = 100mA, resolution = 488µA
$Comp
L VCC #PWR02
U 1 1 593B2CDF
P 3800 3200
F 0 "#PWR02" H 3800 3050 50  0001 C CNN
F 1 "VCC" H 3800 3350 50  0000 C CNN
F 2 "" H 3800 3200 50  0000 C CNN
F 3 "" H 3800 3200 50  0000 C CNN
	1    3800 3200
	1    0    0    -1  
$EndComp
$Comp
L MAX4372 U1
U 1 1 5EFEFEF3
P 4500 3400
F 0 "U1" H 4950 3050 60  0000 C CNN
F 1 "MAX4372" H 4950 3550 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 4950 3150 60  0001 C CNN
F 3 "" H 4950 3150 60  0000 C CNN
	1    4500 3400
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 593B1B80
P 4300 4250
F 0 "C1" H 4325 4350 50  0000 L CNN
F 1 "100nF" H 4325 4150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4338 4100 50  0001 C CNN
F 3 "" H 4300 4250 50  0000 C CNN
	1    4300 4250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5EFF0196
P 4300 4500
F 0 "#PWR03" H 4300 4250 50  0001 C CNN
F 1 "GND" H 4300 4350 50  0000 C CNN
F 2 "" H 4300 4500 50  0000 C CNN
F 3 "" H 4300 4500 50  0000 C CNN
	1    4300 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3600 4500 3600
Wire Wire Line
	4150 3400 3700 3400
Wire Wire Line
	3700 4000 4800 4000
Wire Wire Line
	5500 4000 5100 4000
Wire Wire Line
	5500 3400 5500 4000
Wire Wire Line
	3700 3800 5500 3800
Connection ~ 4000 3600
Wire Wire Line
	3800 3200 3800 3500
Wire Wire Line
	3800 3500 3700 3500
Wire Wire Line
	3700 3900 3800 3900
Wire Wire Line
	3800 3900 3800 4000
Connection ~ 3800 4000
Wire Wire Line
	3700 3700 3800 3700
Wire Wire Line
	3800 3700 3800 3800
Connection ~ 3800 3800
Wire Wire Line
	4000 3600 4000 4100
Wire Wire Line
	4300 4400 4300 4500
Wire Wire Line
	4300 3300 4300 4100
Connection ~ 4300 3400
Wire Wire Line
	4150 3400 4150 3500
Wire Wire Line
	4150 3500 4500 3500
Wire Wire Line
	4700 4000 4700 4200
Wire Wire Line
	4700 4200 5550 4200
Wire Wire Line
	5550 4200 5550 3600
Wire Wire Line
	5550 3600 5400 3600
Connection ~ 4700 4000
Wire Wire Line
	5500 3400 5400 3400
Connection ~ 5500 3800
Wire Wire Line
	4300 3400 4500 3400
Wire Wire Line
	4300 3300 3800 3300
Connection ~ 3800 3300
$EndSCHEMATC
