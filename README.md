# MAX4372 MODULE

The purpose of this page is to explain step by step the realization of a current monitor based on MAX4372.

The board uses the following components :

 * a MAX4372
 * a shunt resistor
 * a 100nF capacitor

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.11.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/07/lina138-current-monitor.html

![Screenshot](https://1.bp.blogspot.com/-hNRo1j6llGk/Xv8hBWUIWdI/AAAAAAAAFY8/QCJuUOmV2HcoGX19tME-NUu3TRnbOtTnQCK4BGAsYHg/s320/max4372-module.jpg)

