
#define VREF          1.073
#define SHUNT         0.100
#define MAX4372_GAIN  50

void setup()
{
  Serial.begin(115200);
  analogReference(INTERNAL);
}

void loop()
{
  unsigned int adc = analogRead(A0);
  Serial.print(F("ADC: ")); Serial.println(adc);
  float voltage = adc * VREF / 1023;
  Serial.print(F("voltage: ")); Serial.print(voltage, 3);  Serial.println(" V");
  float current = voltage / SHUNT / MAX4372_GAIN;
  Serial.print(F("current: ")); Serial.print(current, 3); Serial.println(" A");
  delay(2000);
}
